import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { PhotoItemComponent } from './components/photo-item/photo-item.component';
import { GalleryService } from "./services/gallery.service";
import { InputSwitchComponent } from './components/input-switch/input-switch.component';

@NgModule({
  declarations: [
    AppComponent,
    GalleryComponent,
    PhotoItemComponent,
    InputSwitchComponent
  ],
  imports: [
    BrowserModule, HttpClientJsonpModule, HttpClientModule,
  ],
  providers: [GalleryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
