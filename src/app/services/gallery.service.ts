import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GalleryService {
  apiUrl = 'https://aukro.cz/backend/api/homepage'; //without intropart
  proxyUrl = 'http://localhost:8080/'
  localApiUrl = 'http://localhost:3000/introPart'

  constructor(private http: HttpClient) {
  }

  getGalleryData() {
    //http proxy request
    // return this.http.get(`${this.proxyUrl}${this.apiUrl}`);
    return this.http.get(`${this.localApiUrl}`);
  }
}
