export interface ApiDataEntity0 {
  type: string;
  moCode: string;
  content: Content;
}
export interface Content {
  bannerPlacement?: BannerPlacement | null;
  title?: string | null;
  items?: (ItemsEntity)[] | null;
}
export interface BannerPlacement {
  placementCode: string;
  placementName: string;
  bannerPositions?: (BannerPositionsEntity)[] | null;
}
export interface BannerPositionsEntity {
  positionName: string;
  positionIndex: number;
  bannerDeployments?: (BannerDeploymentsEntity)[] | null;
}
export interface BannerDeploymentsEntity {
  deploymentId: number;
  bannerName: string;
  promoId: number;
  promoName: string;
  imgUrl: string;
  destinationUrl: string;
  shuffleable: boolean;
  openInNewTab: boolean;
}
export interface ItemsEntity {
  endingTime: string;
  id: number;
  name: string;
  aukroPlus: boolean;
  paymentOnline: boolean;
  buyNowPrice: number;
  biddingPrice: number;
  freeShipping: boolean;
  priceWithShipping: number;
  images: Images;
  itemTypeEnumCode: string;
  seoUrl: string;
  paymentViaAukro: boolean;
  statusAdultContent: boolean;
}
export interface Images {
  lists: Lists;
}
export interface Lists {
  small?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
  original?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
  large?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
  medium?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
}
export interface SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity {
  id: number;
  position: number;
  titleImage: boolean;
  url: string;
  thumbnailReady: boolean;
  aukroImage: boolean;
}
export interface ApiDataEntity1 {
  bannerPlacement?: BannerPlacement | null;
  title?: string | null;
  items?: (ItemsEntity)[] | null;
}
export interface BannerPlacement {
  placementCode: string;
  placementName: string;
  bannerPositions?: (BannerPositionsEntity)[] | null;
}
export interface BannerPositionsEntity {
  positionName: string;
  positionIndex: number;
  bannerDeployments?: (BannerDeploymentsEntity)[] | null;
}
export interface BannerDeploymentsEntity {
  deploymentId: number;
  bannerName: string;
  promoId: number;
  promoName: string;
  imgUrl: string;
  destinationUrl: string;
  shuffleable: boolean;
  openInNewTab: boolean;
}
export interface ItemsEntity {
  endingTime: string;
  id: number;
  name: string;
  aukroPlus: boolean;
  paymentOnline: boolean;
  buyNowPrice: number;
  biddingPrice: number;
  freeShipping: boolean;
  priceWithShipping: number;
  images: Images;
  itemTypeEnumCode: string;
  seoUrl: string;
  paymentViaAukro: boolean;
  statusAdultContent: boolean;
}
export interface Images {
  lists: Lists;
}
export interface Lists {
  small?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
  original?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
  large?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
  medium?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
}
export interface SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity {
  id: number;
  position: number;
  titleImage: boolean;
  url: string;
  thumbnailReady: boolean;
  aukroImage: boolean;
}
export interface ApiDataEntity2 {
  placementCode: string;
  placementName: string;
  bannerPositions?: (BannerPositionsEntity)[] | null;
}
export interface BannerPositionsEntity {
  positionName: string;
  positionIndex: number;
  bannerDeployments?: (BannerDeploymentsEntity)[] | null;
}
export interface BannerDeploymentsEntity {
  deploymentId: number;
  bannerName: string;
  promoId: number;
  promoName: string;
  imgUrl: string;
  destinationUrl: string;
  shuffleable: boolean;
  openInNewTab: boolean;
}
export interface ApiDataEntity3 {
  positionName: string;
  positionIndex: number;
  bannerDeployments?: (BannerDeploymentsEntity)[] | null;
}
export interface BannerDeploymentsEntity {
  deploymentId: number;
  bannerName: string;
  promoId: number;
  promoName: string;
  imgUrl: string;
  destinationUrl: string;
  shuffleable: boolean;
  openInNewTab: boolean;
}
export interface ApiDataEntity4 {
  deploymentId: number;
  bannerName: string;
  promoId: number;
  promoName: string;
  imgUrl: string;
  destinationUrl: string;
  shuffleable: boolean;
  openInNewTab: boolean;
}
export interface ApiDataEntity5 {
  endingTime: string;
  id: number;
  name: string;
  aukroPlus: boolean;
  paymentOnline: boolean;
  buyNowPrice: number;
  biddingPrice: number;
  freeShipping: boolean;
  priceWithShipping: number;
  images: Images;
  itemTypeEnumCode: string;
  seoUrl: string;
  paymentViaAukro: boolean;
  statusAdultContent: boolean;
}
export interface Images {
  lists: Lists;
}
export interface Lists {
  small?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
  original?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
  large?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
  medium?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
}
export interface SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity {
  id: number;
  position: number;
  titleImage: boolean;
  url: string;
  thumbnailReady: boolean;
  aukroImage: boolean;
}
export interface ApiDataEntity6 {
  lists: Lists;
}
export interface Lists {
  small?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
  original?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
  large?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
  medium?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
}
export interface SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity {
  id: number;
  position: number;
  titleImage: boolean;
  url: string;
  thumbnailReady: boolean;
  aukroImage: boolean;
}
export interface ApiDataEntity7 {
  small?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
  original?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
  large?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
  medium?: (SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity)[] | null;
}
export interface SmallEntityOrOriginalEntityOrLargeEntityOrMediumEntity {
  id: number;
  position: number;
  titleImage: boolean;
  url: string;
  thumbnailReady: boolean;
  aukroImage: boolean;
}
export interface ApiDataEntity8 {
  id: number;
  position: number;
  titleImage: boolean;
  url: string;
  thumbnailReady: boolean;
  aukroImage: boolean;
}
export type ApiData = (ApiDataEntity0)[] | null;

