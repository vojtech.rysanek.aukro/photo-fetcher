import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-input-switch',
  templateUrl: './input-switch.component.html',
  styleUrls: ['./input-switch.component.scss']
})
export class InputSwitchComponent implements OnInit {
  @Output() switcherState = new EventEmitter<boolean>();
  state = false;

  onSwitch() {
    this.state = !this.state;
    this.switcherState.emit(this.state);
  }

  constructor() { }

  ngOnInit(): void {
  }
}
