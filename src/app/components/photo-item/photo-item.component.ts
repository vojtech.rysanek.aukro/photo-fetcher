import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-photo-item',
  templateUrl: './photo-item.component.html',
  styleUrls: ['./photo-item.component.scss']
})
export class PhotoItemComponent implements OnInit {
  @Input() content?: string;
  @Input() imgUrl?: string;
  @Input() isGrayScaled = false;
  constructor() { }

  ngOnInit(): void {
  }

  getImgClass(){
    return {'is-gray-scaled': this.isGrayScaled}
  }
}
