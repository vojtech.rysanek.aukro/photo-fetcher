import {Component, OnInit} from '@angular/core';
import {GalleryService} from "../../services/gallery.service";
import {Observable, of} from "rxjs";
import {map, filter, skip, take, toArray} from 'rxjs/operators';
import { Content, ApiDataEntity5, ItemsEntity } from '../../interfaces/api-data-interfaces'

export interface galleryItem{
  name: string;
  image: string;
}

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent {
  apiData$: Observable<Content>;
  apiDataItem: ApiDataEntity5 [] = [];
  apiFinalData$?: Observable<galleryItem[]>;
  isGray = false;

  constructor(private galleryService: GalleryService) {
    this.apiData$ = this.galleryService.getGalleryData();
    const filtered$ = this.apiData$.pipe(
      map((x: any) => x[0]),
      map((x: any) => x[0].content.items),
    );

    filtered$.subscribe(x => {
      this.apiDataItem = x;
      this.getRandomDataToItems();
    },
      (error) => {
        console.error(`Error in Api subsriptions`);
        console.log(error);
        alert(`Data doesn't load. Did you start local api first? Pls check Readme.txt`);
      });
  }

  async getRandomDataToItems() {
    const randomInt = await this.getRandomInt(13);
    this.apiFinalData$ = of(...this.apiDataItem).pipe(skip(randomInt), take(4),
      map(x => {
        return { name: x.name, image: x.images.lists.large![0].url }
      }), toArray());
  }

  trackByFn(index: number, item: any) {
    return index;
  }

  setGray(val: boolean) {
    this.isGray = val;
  }

  private getRandomInt(max: number) {
    return Math.floor(Math.random() * max);
  }
}
